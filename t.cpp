#include <set>

#include "gmock/gmock.h"
#include "wpp.hpp"

using namespace std;


TEST(Trivia, TestsRun) {
}

TEST(Syntax, SimpleProgram) {
    auto eVarF = make_unique<EVar>("f");                    // f
    auto eVarX = make_unique<EVar>("x");                    // x

    auto eLit1 = make_unique<ELit>(                         // 1
            make_unique<LInt>(1));

    auto eAppF1a = make_unique<EApp>(                       // f x
            make_unique<EVar>("f"),
            make_unique<EVar>("x"));
    auto eAppF1b = make_unique<EApp>(
            eVarF->clone(),
            eVarX->clone());

    auto eAbsFa = make_unique<EAbs>(                        // λx.x
            make_unique<EVar>("x"),
            make_unique<EVar>("x"));
    auto eAbsFb = make_unique<EAbs>(
            eVarX->clone(),
            eVarX->clone());

    auto eLeta = make_unique<ELet>(                         // let f=λx.x in f 1
            make_unique<EVar>("f"),
            make_unique<EAbs>(make_unique<EVar>("x"),
                              make_unique<EVar>("x")),
            make_unique<EApp>(make_unique<EVar>("f"),
                              make_unique<EVar>("x")));
    auto eLetb = make_unique<ELet>(                         // let f=λx.x in f 1
            eVarF->clone(),
            eAbsFa->clone(),
            eAppF1b->clone());
}

TEST(Syntax, SimpleTypes) {
    auto tVarA = make_unique<TVar>("a");  // a
    auto tInt = make_unique<TInt>();      // Int
    auto tBool = make_unique<TBool>();    // Bool
    auto tFuna = make_unique<TFun>(       // a -> Int
            make_unique<TVar>("a"),
            make_unique<TInt>());
    auto tFunb = make_unique<TFun>(
            tVarA->clone(),
            tInt->clone());
}

TEST(Syntax, SimpleTypeSchemes) {
    auto s = make_unique<Scheme>(                      // ∀a. a -> Int
            vector<string> {"a"},
            make_unique<TFun>(make_unique<TVar>("a"),
                              make_unique<TInt>()));
}

TEST(TypesImplForType, FTV) {
    auto tVarA = make_unique<TVar>("a");  // a

    auto tInt = make_unique<TInt>();      // Int

    auto tFuna = make_unique<TFun>(       // a -> Int
            make_unique<TVar>("a"),
            make_unique<TInt>());
    auto tFunb = make_unique<TFun>(
            tVarA->clone(),
            tInt->clone());

    ASSERT_TRUE(tVarA->ftv() == set<string>{"a"});
    ASSERT_TRUE(tInt->ftv() == set<string>{});
    ASSERT_TRUE(tFuna->ftv() == set<string>{"a"});
    ASSERT_TRUE(tFunb->ftv() == set<string>{"a"});
}

TEST(TypesImplForType, Apply_1) {
    auto tVarA = make_unique<TVar>("a");  // a
    auto tInt = make_unique<TInt>();      // Int
    auto tFun = make_unique<TFun>(
            tVarA->clone(),               // a -> Int
            tInt->clone());

    auto subst = Subst {
            {"a", make_shared<TInt>()}
    };

    auto tFun2 = tFun->apply(subst);
    ASSERT_TRUE(tFun->ftv() == set<string>{"a"});
    ASSERT_TRUE(tFun2->ftv() == set<string>{});
}

TEST(TypesImplForType, Apply_2) {
    auto tFun = make_unique<TFun>(         // a -> Int
            make_unique<TVar>("a"),
            make_unique<TInt>());

    auto subst = Subst {
            {"a", make_shared<TVar>("b")}
    };

    auto tFun2 = tFun->apply(subst);
    ASSERT_TRUE(tFun->ftv() == set<string>{"a"});
    ASSERT_TRUE(tFun2->ftv() == set<string>{"b"});
    ASSERT_TRUE(tFun->from->ftv() == set<string>{"a"});
    ASSERT_TRUE(static_cast<TFun *>(tFun2.get())->from->ftv() == set<string>{"b"});
}

TEST(TypesImplForScheme, FTV) {
    auto tVarA = make_unique<TVar>("a");     // a
    auto tInt = make_unique<TInt>();         // Int
    auto tFun = make_unique<TFun>(           // a -> Int
            tVarA->clone(),
            tInt->clone());

    auto s = make_unique<Scheme>(            // ∀a. a -> Int
            vector<string> {tVarA->typeVar},
            tFun->clone());

    ASSERT_TRUE(s->ftv() == set<string>{});
}

TEST(TypesImplForScheme, Apply) {
    auto tVarA = make_unique<TVar>("a");  // a
    auto tInt = make_unique<TInt>();      // Int
    auto tFun = make_unique<TFun>(        // a -> Int
            tVarA->clone(),
            tInt->clone());

    auto sA = make_unique<Scheme>(        // ∀a. a -> Int
            vector<string> {"a"},
            tFun->clone());
    auto sB = make_unique<Scheme>(        // ∀.  a -> Int
            vector<string> {},
            tFun->clone());

    auto subst = Subst {
            {"a", make_shared<TVar>("b")}
    };

    auto sA2 = sA->apply(subst);
    auto sB2 = sB->apply(subst);

    ASSERT_TRUE(sA->ftv() == set<string>{});
    ASSERT_TRUE(sA2->ftv() == set<string>{});
    ASSERT_TRUE(sB->ftv() == set<string>{"a"});
    ASSERT_TRUE(sB2->ftv() == set<string>{"b"});
}

TEST(Subst, ComposeSubst) {
    auto s1 = Subst {                  // s1 = {a->b}
            {"a", make_shared<TVar>("b")}
    };
    auto s2 = Subst {                  // s2 = {b->c}
            {"b", make_shared<TVar>("c")}
    };
    auto s3 = Subst {                  // s3 = {a->d}
            {"a", make_shared<TVar>("d")}
    };
    auto s4 = Subst {                  // s4 = {b->e}
            {"b", make_shared<TVar>("e")}
    };

    ASSERT_EQ(composeSubst(s1, s2).size(), 2);
    ASSERT_EQ(composeSubst(s2, s1).size(), 2);

    // Quickcheck-style tests

    vector<TVar> tvars {
            TVar{"a"}, TVar{"b"}, TVar{"c"}, TVar{"d"}, TVar{"e"}, TVar{"f"}
    };

    vector<Subst> allSubsts {
            s1, s2, s3, s4
    };

    for(const Subst sA : allSubsts) {
        for(const Subst sB : allSubsts) {
            for(const TVar v : tvars) {
                // assert that (sA∘sB)(x) == sA(sB(x))
                pType left_side = v.apply(composeSubst(sA, sB));
                pType right_side = v.apply(sB)->apply(sA);
                ASSERT_EQ(dynamic_cast<TVar*>(left_side.get())->typeVar,
                          dynamic_cast<TVar*>(right_side.get())->typeVar);
            }
        }
    }
}

TEST(Types, ID) {
    ASSERT_EQ(TVar{"a"}.id()                     , Type::TVarID);
    ASSERT_NE(TInt{}.id()                        , Type::TVarID);
    ASSERT_NE(TBool{}.id()                       , Type::TVarID);
    ASSERT_NE((TFun{new TInt{}, new TInt{}}.id()), Type::TVarID);

    ASSERT_NE(TVar{"a"}.id()                     , Type::TIntID);
    ASSERT_EQ(TInt{}.id()                        , Type::TIntID);
    ASSERT_NE(TBool{}.id()                       , Type::TIntID);
    ASSERT_NE((TFun{new TInt{}, new TInt{}}.id()), Type::TIntID);

    ASSERT_NE(TVar{"a"}.id()                     , Type::TBoolID);
    ASSERT_NE(TInt{}.id()                        , Type::TBoolID);
    ASSERT_EQ(TBool{}.id()                       , Type::TBoolID);
    ASSERT_NE((TFun{new TInt{}, new TInt{}}.id()), Type::TBoolID);

    ASSERT_NE(TVar{"a"}.id()                     , Type::TFunID);
    ASSERT_NE(TInt{}.id()                        , Type::TFunID);
    ASSERT_NE(TBool{}.id()                       , Type::TFunID);
    ASSERT_EQ((TFun{new TInt{}, new TInt{}}.id()), Type::TFunID);
}

::testing::AssertionResult AssertSubstitutionsEqual(const char* l_expr,
                                                    const char* r_expr,
                                                    const Subst l,
                                                    const Subst r) {
    if(l.size() != r.size()) {
        return ::testing::AssertionFailure()
            << "Substitutions have different size.\n"\
            "\tL=" << l_expr << "\n"\
            "\tR=" << r_expr;

    }

    for(auto &l_elem : l) {
        auto search = r.find(l_elem.first);
        if(search == r.end()) {
            return ::testing::AssertionFailure()
                << "Left side contains `" << l_elem.first
                                          << " :-> "
                                          << "??"
                                          << "` which is not available on the right side.\n"\
                "\tL=" << l_expr << "\n"\
                "\tR=" << r_expr;
        }
    }
    return ::testing::AssertionFailure() << "unimplemented.\n\tL=" << l_expr << "\n\tR=" << r_expr;
}

TEST(MGU, TypeEqualityBasics) {
    pType tv_a1 = pType(new TVar{"a"});
    pType tv_a2 = pType(new TVar{"a"});
    pType tv_b = pType(new TVar{"b"});
    ASSERT_EQ(*tv_a1, *tv_a1);
    ASSERT_EQ(*tv_a1, *tv_a2);
    ASSERT_NE(*tv_a1, *tv_b);
}

TEST(MGU, Trivia) {
    auto tv_a = make_unique<TVar>("a");
    auto tv_b = make_unique<TVar>("b");
    auto tf_a = make_unique<TFun>(new TVar("c"), new TVar("d"));

    ASSERT_PRED_FORMAT2(AssertSubstitutionsEqual,
                        mgu(*tv_a, *tv_b),
                        (Subst{
                            {"a", make_shared<TVar>("b")}
                        }));
    //ASSERT_TRUE((mgu(*tv_a, *tf_a) == map<string, spType>{}));
    //ASSERT_TRUE((mgu(*tf_a, *tf_a) == map<string, spType>{}));
    //ASSERT_TRUE((mgu(*tf_a, *tv_a) == map<string, spType>{}));
}


struct TypeEnvTests : public testing::Test {
    pTFun tFun;
    pTFun tFun2;
    spScheme schemeA;
    spScheme schemeB;
    spScheme schemeC;
    Subst subst;
    pTypeEnv te;

    TypeEnvTests()
            : tFun(
                  make_unique<TFun>(
                      make_unique<TVar>("a"),
                      make_unique<TInt>())),
              tFun2(
                  make_unique<TFun>(
                      make_unique<TVar>("e"),
                      make_unique<TInt>())),
              schemeA(
                  make_shared<Scheme>(   // ∀. a -> Int
                      vector<string> {},
                      tFun->clone())),
              schemeB(
                  make_shared<Scheme>(   // ∀a. a -> Int
                      vector<string> {"a"},
                      tFun->clone())),
              schemeC(
                  make_shared<Scheme>(   // ∀c. a -> Int
                      vector<string> {"c", "d"},
                      tFun->clone())),
              subst(
                  map<string,spType>{
                      {"a", make_shared<TVar>("b")}
                  }),
              te(
                  make_unique<TypeEnv>(
                      map<string, spScheme>{ // Γ = {
                         {"f1", schemeA},   //   f1 : ∀. a -> Int
                         {"f2", schemeB},   //   f2 : ∀a. a -> Int
                         {"f3", schemeC},   //   f3 : ∀c,d. e -> Int
                      }))                    // }
    {}
};

TEST_F(TypeEnvTests, TypeEnv_Types_Apply) {
    auto te2 = te->apply(subst);

    ASSERT_EQ(te2->env.size(), 3);
    ASSERT_EQ(te2->env.at("f1")->typeVars.size(), 0);
    ASSERT_EQ(Type::as<TVar>(
                  Type::as<TFun>(
                      te2->env.at("f1")
                  )->from.get()
              )->typeVar,
              "b");

    ASSERT_EQ(te2->env.at("f2")->typeVars.size(), 1);
    ASSERT_EQ(te2->env.at("f2")->typeVars[0], "a");
    ASSERT_EQ(Type::as<TVar>(
                  Type::as<TFun>(
                      te2->env.at("f2")
                  )->from.get()
              )->typeVar,
              "a");

    ASSERT_EQ(te2->env.at("f3")->typeVars.size(), 2);
    ASSERT_EQ(te2->env.at("f3")->typeVars[0], "c");
    ASSERT_EQ(Type::as<TVar>(
                  Type::as<TFun>(
                      te2->env.at("f3")
                  )->from.get()
              )->typeVar,
              "b");
}

TEST_F(TypeEnvTests, Generatlize_Trivia) {
    auto scheme_generalized = generalize(*te, *tFun2);
    ASSERT_EQ(scheme_generalized->typeVars.size(),
              1);
    ASSERT_EQ(scheme_generalized->typeVars[0],
              "e");
    ASSERT_EQ(scheme_generalized->type
                                ->as<TFun>()->from
                                ->as<TVar>()->typeVar,
              "e");
    ASSERT_EQ(scheme_generalized->type
                                ->as<TFun>()->to
                                ->id(),
              Type::TIntID);
}


int main(int argc, char **argv) {
    ::testing::InitGoogleMock(&argc, argv);
    return RUN_ALL_TESTS();
}
