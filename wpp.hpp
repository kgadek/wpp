#pragma once
#include <string>
#include <vector>
#include <memory>
#include <set>
#include <map>
#include <algorithm>

using namespace std;


struct Lit;     typedef unique_ptr<Lit>     pLit;     typedef shared_ptr<Lit>     spLit;
struct LInt;    typedef unique_ptr<LInt>    pLInt;    typedef shared_ptr<LInt>    spLInt;
struct Exp;     typedef unique_ptr<Exp>     pExp;     typedef shared_ptr<Exp>     spExp;
struct EVar;    typedef unique_ptr<EVar>    pEVar;    typedef shared_ptr<EVar>    spEVar;
struct ELit;    typedef unique_ptr<ELit>    pELit;    typedef shared_ptr<ELit>    spELit;
struct EApp;    typedef unique_ptr<EApp>    pEApp;    typedef shared_ptr<EApp>    spEApp;
struct EAbs;    typedef unique_ptr<EAbs>    pEAbs;    typedef shared_ptr<EAbs>    spEAbs;
struct ELet;    typedef unique_ptr<ELet>    pELet;    typedef shared_ptr<ELet>    spELet;
struct Type;    typedef unique_ptr<Type>    pType;    typedef shared_ptr<Type>    spType;
struct TVar;    typedef unique_ptr<TVar>    pTVar;    typedef shared_ptr<TVar>    spTVar;
struct TInt;    typedef unique_ptr<TInt>    pTInt;    typedef shared_ptr<TInt>    spTInt;
struct TBool;   typedef unique_ptr<TBool>   pTBool;   typedef shared_ptr<TBool>   spTBool;
struct TFun;    typedef unique_ptr<TFun>    pTFun;    typedef shared_ptr<TFun>    spTFun;
struct Scheme;  typedef unique_ptr<Scheme>  pScheme;  typedef shared_ptr<Scheme>  spScheme;
struct TypeEnv; typedef unique_ptr<TypeEnv> pTypeEnv; typedef shared_ptr<TypeEnv> spTypeEnv;

typedef map<string,spType> Subst;

//  _____            _                 _   _
// |  __ \          | |               | | (_)
// | |  | | ___  ___| | __ _ _ __ __ _| |_ _  ___  _ __  ___
// | |  | |/ _ \/ __| |/ _` | '__/ _` | __| |/ _ \| '_ \/ __|
// | |__| |  __/ (__| | (_| | | | (_| | |_| | (_) | | | \__ \
// |_____/ \___|\___|_|\__,_|_|  \__,_|\__|_|\___/|_| |_|___/
//

struct Lit {
    virtual Lit* clone() const = 0;
    // ^ This is fugly, I *really* don't want to deal with raw pointers.
    // What I actually want is the covariance of unique_ptr.
};

struct LInt : public Lit {
    int value;
    LInt(int);
    LInt* clone() const;
};

struct Exp {
    virtual Exp* clone() const = 0;
};

struct EVar : public Exp {
    string varName;

    EVar(string);
    EVar* clone() const;
};

struct ELit : public Exp {
    pLit value;

    ELit(pLit);
    ELit(Lit *);

    ELit* clone() const;
};

struct EApp : public Exp {
    pExp fun;
    pExp arg;

    EApp(pExp, pExp);
    EApp(Exp *, Exp *);

    EApp* clone() const;
};

struct EAbs : public Exp {
    pExp var;
    pExp res;

    EAbs(pExp, pExp);
    EAbs(Exp *, Exp *);

    EAbs* clone() const;
};

struct ELet : public Exp {
    pEVar var;
    pExp value;
    pExp expr;

    ELet(pEVar, pExp, pExp);
    ELet(EVar *, Exp *, Exp *);

    ELet* clone() const;
};

template<class T>
struct Types {
    virtual set<string> ftv() const = 0;
    virtual unique_ptr<T> apply(const map<string, spType>&) const = 0;
};

struct Scheme : Types<Scheme> {
    vector<string> typeVars;
    pType type;

    Scheme(vector<string>, pType);
    Scheme(vector<string>, Type *);
    Scheme* clone() const;

    set<string> ftv() const;
    pScheme apply(const map<string, spType>&) const;
};

struct Type : public Types<Type> {
    enum TypeID {
        TBoolID = 0,
        TIntID = 1,
        TVarID = 2,
        TFunID = 3,
    };

    virtual TypeID id() const = 0; // well, it *is* error prone. But still I'd rather use this than RTTI.

    virtual Type* clone() const = 0;

    template <class T>
    const T* as() const {
        return Type::as<T>(this);
    }

    template <class T>
    static const T* as(const Type *t) {
        return static_cast<const T*>(t);
    }

    template <class T>
    static T* as(Type *t) {
        return static_cast<T*>(t);
    }

    template <class T>
    static T* as(spType t) {
        return static_cast<T*>(t.get());
    }

    template <class T>
    static T* as(spScheme sch) {
        return static_cast<T*>(sch->type.get());
    }

    virtual Subst mgu_visitor(const Type &that) const = 0;
    virtual Subst mgu(const TVar &)             const = 0;
    virtual Subst mgu(const TInt &)             const = 0;
    virtual Subst mgu(const TBool &)            const = 0;
    virtual Subst mgu(const TFun &)             const = 0;

    virtual bool operator==(const Type &) const = 0;
    virtual bool equal(const TVar &)      const;
    virtual bool equal(const TInt &)      const;
    virtual bool equal(const TBool &)     const;
    virtual bool equal(const TFun &)      const;

    bool operator!=(const Type &that) const;
};

struct TVar : public Type {
    string typeVar;

    TVar(string);

    TVar* clone() const;
    TypeID id() const {
        return TypeID::TVarID;
    }

    set<string> ftv() const;
    pType apply(const map<string, spType>&) const;

    Subst mgu_visitor(const Type &that) const;
    Subst mgu(const TVar &)  const;
    Subst mgu(const TInt &)  const;
    Subst mgu(const TBool &) const;
    Subst mgu(const TFun &)  const;

    bool operator==(const Type &that) const;
    bool equal(const TVar &that) const;
};

struct TInt : public Type {
    TInt* clone() const;
    TypeID id() const {
        return TypeID::TIntID;
    }

    set<string> ftv() const;
    pType apply(const map<string, spType>&) const;

    Subst mgu_visitor(const Type &that) const;
    Subst mgu(const TVar &)  const;
    Subst mgu(const TInt &)  const;
    Subst mgu(const TBool &) const;
    Subst mgu(const TFun &)  const;

    bool operator==(const Type &that) const;
    bool equal(const TInt &) const;
};

struct TBool : public Type {
    TBool* clone() const;
    TypeID id() const {
        return TypeID::TBoolID;
    }

    set<string> ftv() const;
    pType apply(const map<string, spType>&) const;

    Subst mgu_visitor(const Type &that) const;
    Subst mgu(const TVar &)  const;
    Subst mgu(const TInt &)  const;
    Subst mgu(const TBool &) const;
    Subst mgu(const TFun &)  const;

    bool operator==(const Type &that) const;
    bool equal(const TBool &) const;
};

struct TFun : public Type {
    pType from;
    pType to;

    TFun(pType, pType);
    TFun(Type *, Type *);

    TFun* clone() const;
    TypeID id() const {
        return TypeID::TFunID;
    }

    set<string> ftv() const;
    pType apply(const map<string, spType>&) const;

    Subst mgu_visitor(const Type &that) const;
    Subst mgu(const TVar &)  const;
    Subst mgu(const TInt &)  const;
    Subst mgu(const TBool &) const;
    Subst mgu(const TFun &)  const;

    bool operator==(const Type &that) const;
    bool equal(const TFun &that) const;
};

struct TypeEnv : Types<TypeEnv> {
    map<string, spScheme> env;

    TypeEnv(map<string, spScheme>);

    TypeEnv* clone() const;

    set<string> ftv() const;
    pTypeEnv apply(const map<string,spType>&) const;
};

Subst composeSubst(const Subst s1, const Subst s2);

pScheme generalize(const TypeEnv &, const Type &);

inline Subst mgu(const Type &, const Type &);

//  _____        __ _       _ _   _
// |  __ \      / _(_)     (_) | (_)
// | |  | | ___| |_ _ _ __  _| |_ _  ___  _ __  ___
// | |  | |/ _ \  _| | '_ \| | __| |/ _ \| '_ \/ __|
// | |__| |  __/ | | | | | | | |_| | (_) | | | \__ \
// |_____/ \___|_| |_|_| |_|_|\__|_|\___/|_| |_|___/
//




LInt::LInt(int _value)
        : value(_value) {}

LInt* LInt::clone() const {
    return new LInt{value};
}

EVar::EVar(string _varName)
        : varName(_varName) {}

EVar* EVar::clone() const {
    return new EVar{varName};
}

ELit::ELit(pLit _value)
        : value(move(_value)) {}

ELit::ELit(Lit *_value)
        : value(_value) {}

ELit* ELit::clone() const {
    return new ELit{value->clone()};
}

EApp::EApp(pExp _fun, pExp _arg)
        : fun(move(_fun)), arg(move(_arg)) {}

EApp::EApp(Exp *_fun, Exp *_arg)
        : fun(_fun), arg(_arg) {}

EApp* EApp::clone() const {
    return new EApp{fun->clone(), arg->clone()};
}

EAbs::EAbs(pExp _var, pExp _res)
        : var(move(_var)), res(move(_res)) {}

EAbs::EAbs(Exp *_var, Exp *_res)
        : var(_var), res(_res) {}

EAbs* EAbs::clone() const {
    return new EAbs{var->clone(), res->clone()};
}

ELet::ELet(pEVar _var, pExp _value, pExp _expr)
        : var(move(_var)), value(move(_value)), expr(move(_expr)) {}

ELet::ELet(EVar *_var, Exp *_value, Exp *_expr)
        : var(_var), value(_value), expr(_expr) {}

ELet* ELet::clone() const {
    return new ELet{var->clone(), value->clone(), expr->clone()};
}

TVar::TVar(string _typeVar)
        : typeVar(move(_typeVar)) {}

TVar* TVar::clone() const {
    return new TVar{typeVar};
}

set<string> TVar::ftv() const {
    return {typeVar};
}

pType TVar::apply(const map<string, spType> &subst) const {
    auto search = subst.find(typeVar);
    if(search == subst.end()) {
        return make_unique<TVar>(typeVar);
    } else {
        return unique_ptr<Type>(search->second->clone());
    }
}


TInt* TInt::clone() const {
    return new TInt{};
}

set<string> TInt::ftv() const {
    return {};
}

pType TInt::apply(const map<string, spType>&) const {
    return make_unique<TInt>();
}

TBool* TBool::clone() const {
    return new TBool{};
}

set<string> TBool::ftv() const {
    return {};
}

pType TBool::apply(const map<string, spType>&) const {
    return make_unique<TBool>();
}


TFun::TFun(pType _from, pType _to)
        : from(move(_from)), to(move(_to)) {}

TFun::TFun(Type *_from, Type *_to)
        : from(_from), to(_to) {}

TFun* TFun::clone() const {
    return new TFun{from->clone(), to->clone()};
}

set<string> TFun::ftv() const {
    set<string> result {};
    auto from_ftv = from->ftv();
    result.insert(from_ftv.begin(), from_ftv.end());
    auto to_ftv = to->ftv();
    result.insert(to_ftv.begin(), to_ftv.end());
    return result;
}

pType TFun::apply(const map<string, spType> &subst) const {
    auto from_new = from->apply(subst);
    auto to_new = to->apply(subst);
    return make_unique<TFun>(move(from_new), move(to_new));
}

Subst TVar::mgu_visitor(const Type &that) const {
    return that.mgu(*this);
}

Subst TInt::mgu_visitor(const Type &that) const {
    return that.mgu(*this);
}

Subst TBool::mgu_visitor(const Type &that) const {
    return that.mgu(*this);
}

Subst TFun::mgu_visitor(const Type &that) const {
    return that.mgu(*this);
}

Subst TVar::mgu(const TVar &that)  const {
    if(typeVar == that.typeVar) {
        return Subst {};
    }

    auto that_ftv = that.ftv();
    auto search = that_ftv.find(typeVar);
    if(search != that_ftv.end()) {
        throw "occur check fails";
    }

    return Subst {
        {typeVar, spType(that.clone())}
    };
}

Subst TVar::mgu(const TInt &)  const {
    //cout << "TVar::TInt"  << endl;
}

Subst TVar::mgu(const TBool &) const {
    //cout << "TVar::TBool" << endl;
}

Subst TVar::mgu(const TFun &)  const {
    //cout << "TVar::TFun"  << endl;
}


Subst TInt::mgu(const TVar &)  const {
    //cout << "TInt::TVar"  << endl;
}

Subst TInt::mgu(const TInt &)  const {
    //cout << "TInt::TInt"  << endl;
}

Subst TInt::mgu(const TBool &) const {
    //cout << "TInt::TBool" << endl;
}

Subst TInt::mgu(const TFun &)  const {
    //cout << "TInt::TFun"  << endl;
}


Subst TBool::mgu(const TVar &)  const {
    //cout << "TBool::TVar"  << endl;
}

Subst TBool::mgu(const TInt &)  const {
    //cout << "TBool::TInt"  << endl;
}

Subst TBool::mgu(const TBool &) const {
    //cout << "TBool::TBool" << endl;
}

Subst TBool::mgu(const TFun &)  const {
    //cout << "TBool::TFun"  << endl;
}


Subst TFun::mgu(const TVar &)  const {
    //cout << "TFun::TVar"  << endl;
}

Subst TFun::mgu(const TInt &)  const {
    //cout << "TFun::TInt"  << endl;
}

Subst TFun::mgu(const TBool &) const {
    //cout << "TFun::TBool" << endl;
}

Subst TFun::mgu(const TFun &)  const {
    //cout << "TFun::TFun"  << endl;
}

bool Type::equal(const TVar &) const {
    return false;
}

bool Type::equal(const TInt &) const {
    return false;
}

bool Type::equal(const TBool &) const {
    return false;
}

bool Type::equal(const TFun &) const {
    return false;
}


bool Type::operator!=(const Type &that) const {
    return !( this->operator==(that) );
}

bool TVar::operator==(const Type &that) const {
    return that.equal(*this);
}

bool TInt::operator==(const Type &that) const {
    return that.equal(*this);
}

bool TBool::operator==(const Type &that) const {
    return that.equal(*this);
}

bool TFun::operator==(const Type &that) const {
    return that.equal(*this);
}

bool TVar::equal(const TVar &that) const {
    return typeVar == that.typeVar;
}

bool TInt::equal(const TInt &) const {
    return true;
}

bool TBool::equal(const TBool &) const {
    return true;
}

bool TFun::equal(const TFun &that) const {
    return from->operator==(that.from.operator*())
        && to->operator==(that.to.operator*());
}

Scheme::Scheme(vector<string> _typeVars, pType _type)
        : typeVars(_typeVars), type(move(_type)) {}

Scheme::Scheme(vector<string> _typeVars, Type *_type)
        : typeVars(_typeVars), type(_type) {}

Scheme* Scheme::clone() const {
    return new Scheme{typeVars, type->clone()};
}

set<string> Scheme::ftv() const {
    set<string> result {};
    auto type_ftv = type->ftv();
    result.insert(type_ftv.begin(), type_ftv.end());
    for(auto&& typeVar : typeVars) {
        result.erase(typeVar);
    }
    return result;
}

pScheme Scheme::apply(const map<string, spType> &subst_orig) const {
    map<string, spType> subst{subst_orig};
    for(auto&& typeVar : typeVars) {
        subst.erase(typeVar);
    }
    return make_unique<Scheme>(typeVars, type->apply(subst));
}

Subst composeSubst(const Subst s1, const Subst s2) {
    Subst res;
    for(auto&& s2_elem : s2) {
        res.emplace(s2_elem.first,
                    spType(s2_elem.second->apply(s1)));
    }
    for(auto&& s1_elem : s1) {
        res.insert(s1_elem);
    }
    return res;
}

TypeEnv::TypeEnv(map<string, spScheme> _env)
        : env(_env) {}

set<string> TypeEnv::ftv() const {
    set<string> result {};
    for(auto&& env_elem : env) {
        auto semi_res = env_elem.second->ftv();
        result.insert(semi_res.begin(), semi_res.end());
    }
    return result;
}

pTypeEnv TypeEnv::apply(const map<string, spType> &subst) const {
    map<string, spScheme> env_new {};
    for(auto&& env_elem : env) {
        env_new.emplace(env_elem.first,
                        env_elem.second->apply(subst));
    }
    return make_unique<TypeEnv>(env_new);
}

TypeEnv* TypeEnv::clone() const {
    return new TypeEnv{this->env};
}

pScheme generalize(const TypeEnv &env, const Type &t) {
    set<string> vars {};

    auto t_ftv = t.ftv();
    vars.insert(t_ftv.begin(), t_ftv.end());

    auto env_ftv = env.ftv();
    for(auto &&non_free : env_ftv) {
        vars.erase(non_free);
    }

    vector<string> vars_v(vars.size());
    copy(vars.begin(), vars.end(), vars_v.begin());

    return make_unique<Scheme>(vars_v, t.clone());
}

inline Subst mgu(const Type &a, const Type &b) {
    return b.mgu_visitor(a);
}

